# Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
# Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: .b .c .d .o

include config.mk

LIBNAME_STATIC = libsgf.a
LIBNAME_SHARED = libsgf.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

default: library
all: default tests

################################################################################
# Library
################################################################################
SRC = src/sgf_device.c src/sgf_estimator.c src/sgf_scene.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)


.config: Makefile config.mk
	$(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys
	$(PKG_CONFIG) --atleast-version $(S2D_VERSION) s2d
	$(PKG_CONFIG) --atleast-version $(S3D_VERSION) s3d
	$(PKG_CONFIG) --atleast-version $(SSP_VERSION) star-sp
	echo 'config done' > $@

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(INCS) -o $@ $(OBJ) $(LDFLAGS_SO) $(LIBS)

$(LIBNAME_STATIC): libsgf.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libsgf.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.c.d:
	@$(CC) $(CFLAGS_SO) $(INCS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(INCS) -DSGF_SHARED_BUILD -c $< -o $@

################################################################################
# Miscellaneous
################################################################################
pkg:
	sed -e 's#@INCDIR@#$(INCDIR)#g'\
	    -e 's#@LIBDIR@#$(LIBDIR)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S2D_VERSION@#$(S2D_VERSION)#g'\
	    -e 's#@S3D_VERSION@#$(S3D_VERSION)#g'\
	    -e 's#@SSP_VERSION@#$(SSP_VERSION)#g'\
	    sgf.pc.in > sgf.pc

sgf-local.pc: sgf.pc.in config.mk
	sed -e 's#@INCDIR@#./src#g'\
	    -e 's#@LIBDIR@#./#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S2D_VERSION@#$(S2D_VERSION)#g'\
	    -e 's#@S3D_VERSION@#$(S3D_VERSION)#g'\
	    -e 's#@SSP_VERSION@#$(SSP_VERSION)#g'\
	    sgf.pc.in > $@

install: library pkg
	mkdir -p "$(DESTDIR)$(LIBDIR)"
	cp $(LIBNAME) "$(DESTDIR)$(LIBDIR)"
	chmod 755 "$(DESTDIR)$(LIBDIR)/$(LIBNAME)"
	mkdir -p "$(DESTDIR)$(LIBDIR)/pkgconfig"
	cp sgf.pc "$(DESTDIR)$(LIBDIR)/pkgconfig"
	chmod 644 "$(DESTDIR)$(LIBDIR)/pkgconfig/sgf.pc"
	mkdir -p "$(DESTDIR)$(INCDIR)/star"
	cp src/sgf.h "$(DESTDIR)$(INCDIR)/star"
	chmod 644 "$(DESTDIR)$(INCDIR)/star/sgf.h"
	mkdir -p "$(DESTDIR)$(DOCDIR)/star-gf"
	cp COPYING README.md "$(DESTDIR)$(DOCDIR)/star-gf"
	chmod 644 "$(DESTDIR)$(DOCDIR)/star-gf/COPYING"
	chmod 644 "$(DESTDIR)$(DOCDIR)/star-gf/README.md"

uninstall:
	rm -f "$(DESTDIR)$(LIBDIR)/$(LIBNAME)"
	rm -f "$(DESTDIR)$(LIBDIR)/pkgconfig/sgf.pc"
	rm -f "$(DESTDIR)$(INCDIR)/star/sgf.h"
	rm -f "$(DESTDIR)$(DOCDIR)/star-gf/COPYING"
	rm -f "$(DESTDIR)$(DOCDIR)/star-gf/README.md"

clean: clean_tests
	rm -f $(OBJ) $(DEP) sgf.pc libsgf.o .config
	rm -f $(LIBNAME_STATIC) $(LIBNAME_SHARED)

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_sgf_cube.c\
 src/test_sgf_device.c\
 src/test_sgf_estimator.c\
 src/test_sgf_scene.c\
 src/test_sgf_square.c\
 src/test_sgf_tetrahedron.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)
TEST_BIN = $(TEST_SRC:.c=.b)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
TEST_INCS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags rsys sgf-local star-sp) -fopenmp
TEST_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs rsys sgf-local star-sp) -fopenmp

test: tests
	@for i in $(TEST_SRC); do \
	  bin="$$(basename "$${i}" ".c")"; \
	  "./$${bin}" > /dev/null 2>&1 \
	    || >&2 printf '%s: error %s\n' "$${bin}" "$$?"; \
	done
	@echo "Tests done"

tests: library $(TEST_DEP) $(TEST_BIN)
	@$(MAKE) -fMakefile \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) \
	$$(for i in $(TEST_BIN); do echo -f"$${i}"; done) \
	test_bin

.c.b:
	@{ \
	  bin="$$(basename "$<" ".c")"; \
	  printf '%s: %s\n' "$${bin}" $(<:.c=.o); \
	  printf 'test_bin: %s\n' "$${bin}"; \
	} > $@

$(TEST_DEP): config.mk sgf-local.pc
	@$(CC) $(CFLAGS_EXE) $(TEST_INCS)  -MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk sgf-local.pc
	$(CC) $(CFLAGS_EXE) $(TEST_INCS) -c $(@:.o=.c) -o $@

test_sgf_cube \
test_sgf_device \
test_sgf_estimator \
test_sgf_scene \
test_sgf_square \
test_sgf_tetrahedron:
	$(CC) $(CFLAGS_EXE) $(TEST_INCS) -o $@ src/$@.o $(LDFLAGS_EXE) $(TEST_LIBS)

clean_tests:
	rm -f $(TEST_OBJ) $(TEST_DEP) $(TEST_BIN) sgf-local.pc
	for i in $(TEST_SRC); do rm -f "$$(basename "$${i}" ".c")"; done
