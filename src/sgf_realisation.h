/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

/*
 * Generate the gebhart_radiative_path_<2|3>d realisation functions with
 * respect to the SGF_DIMENSIONALITY macro
 */

#ifndef SGF_DIMENSIONALITY

#ifndef SGF_REALISATION_H
#define SGF_REALISATION_H

#include "sgf_scene_c.h"

#include <rsys/float2.h>
#include <rsys/float3.h>
#include <rsys/hash_table.h>

#include <star/s2d.h>
#include <star/s3d.h>
#include <star/ssp.h>

/* How many self intersections are tested on the same ray before an error
 * occurs */
#define NSELF_HITS_MAX 10

/* Monte Carlo estimator */
struct accum {
  double radiative_flux;
  double sqr_radiative_flux;
};

/* Declare the htable_bounce data structure */
#define HTABLE_NAME bounce
#define HTABLE_KEY unsigned /* Primitive ID */
#define HTABLE_DATA double /* Weight */
#include <rsys/hash_table.h>

/* Type of the realisation function. Return RES_BAD_OP on numerical issue. i.e.
 * the radiative path is skipped */
typedef res_T
(*gebhart_radiative_path_T)
  (struct sgf_device* dev,
   struct accum* accums,
   struct accum* accum_infinity,
   struct accum* accum_medium,
   struct ssp_rng* rng,
   struct htable_bounce* path, /* Store the path */
   const double absorption_coef, /* In m^-1  */
   const size_t ispectral_band,
   const size_t primitive_id,
   struct sgf_scene* scn);

static FINLINE void
accum_weight(struct accum* accum, const double weight)
{
  ASSERT(accum);
  accum->radiative_flux += weight;
  accum->sqr_radiative_flux += weight * weight;
}

/*******************************************************************************
 * 2D helper functions
 ******************************************************************************/
static FINLINE void
primitive_get_normal_2d(struct s2d_primitive* prim, float normal[3])
{
  struct s2d_attrib attr;
  const float s = 0;
  ASSERT(prim && normal);
  S2D(primitive_get_attrib(prim, S2D_GEOMETRY_NORMAL, s, &attr));
  ASSERT(attr.type == S2D_FLOAT2);
  f2_normalize(normal, attr.value);
  normal[2] = 0.f;
}

static FINLINE void
primitive_sample_position_2d
  (struct s2d_primitive* prim,
   struct ssp_rng* rng,
   float position[3])
{
  struct s2d_attrib attr;
  float s;
  ASSERT(prim && position);
  S2D(primitive_sample(prim, ssp_rng_canonical_float(rng), &s));
  S2D(primitive_get_attrib(prim, S2D_POSITION, s, &attr));
  ASSERT(attr.type == S2D_FLOAT2);
  f2_set(position, attr.value);
  position[2] = 0.f;
}

static FINLINE void
hit_get_normal_2d(struct s2d_hit* hit, float normal[3])
{
  ASSERT(hit && normal);
  f2_normalize(normal, hit->normal);
  normal[2] = 0.f;
}

/*******************************************************************************
 * 3D helper functions
 ******************************************************************************/
static FINLINE void
primitive_get_normal_3d(struct s3d_primitive* prim, float normal[3])
{
  struct s3d_attrib attr;
  const float st[2] = { 0.f, 0.f };
  ASSERT(prim && normal);
  S3D(primitive_get_attrib(prim, S3D_GEOMETRY_NORMAL, st, &attr));
  ASSERT(attr.type == S3D_FLOAT3);
  f3_normalize(normal, attr.value);
}

static FINLINE void
primitive_sample_position_3d
  (struct s3d_primitive* prim,
   struct ssp_rng* rng,
   float position[3])
{
  struct s3d_attrib attr;
  float st[2];
  float u, v;
  ASSERT(prim && position);

  u = ssp_rng_canonical_float(rng);
  v = ssp_rng_canonical_float(rng);
  S3D(primitive_sample(prim, u, v, st));
  S3D(primitive_get_attrib(prim, S3D_POSITION, st, &attr));
  ASSERT(attr.type == S3D_FLOAT3);
  f3_set(position, attr.value);
}

static FINLINE void
hit_get_normal_3d(struct s3d_hit* hit, float normal[3])
{
  ASSERT(hit && normal);
  f3_normalize(normal, hit->normal);
}

#endif /* SGF_REALISATION_H */

#else /* !SGF_DIMENSIONALITY */

#if SGF_DIMENSIONALITY == 2
  /* Wrap */
  #define sXd(Name) CONCAT(s2d_, Name)
  #define SXD(Name) CONCAT(S2D_, Name)
  #define Xd(Name) CONCAT(Name, _2d)

#elif SGF_DIMENSIONALITY == 3
  /* Wrap */
  #define sXd(Name) CONCAT(s3d_, Name)
  #define SXD(Name) CONCAT(S3D_, Name)
  #define Xd(Name) CONCAT(Name, _3d)

#else
  #error Unexpected dimensionility
#endif

static res_T
Xd(gebhart_radiative_path)
  (struct sgf_device* dev,
   struct accum* accums,
   struct accum* accum_infinity,
   struct accum* accum_medium,
   struct ssp_rng* rng,
   struct htable_bounce* path,
   const double absorption_coef, /* m^-1 */
   const size_t ispectral_band,
   const size_t primitive_id,
   struct sgf_scene* scn)
{
  struct sXd(scene_view)* view;
  struct sXd(hit) hit;
  struct sXd(primitive) prim;
  struct htable_bounce_iterator it, end;
  const double trans_min = 1.e-8; /* Minimum transmissivity threshold */
  double proba_reflec_spec;
  double transmissivity, emissivity, reflectivity, specularity;
  double medium_transmissivity;
#ifndef NDEBUG
  double radiative_flux = 0.0;
#endif
  double infinite_radiative_flux = 0.0;
  double medium_radiative_flux = 0.0;
  float vec0[3]; /* Temporary vector */
  float normal[3]; /* Geometric normal */
  float pos[3]; /* Radiative path position */
  float dir[3]; /* Radiative path direction */
  float range[2]; /* Traced ray range */
  res_T res = RES_OK;
  ASSERT(accums && rng && scn);
  ASSERT(absorption_coef < 0 || accum_medium);
  ASSERT(absorption_coef >= 0 || accum_infinity);

#if SGF_DIMENSIONALITY == 2
  view = scn->geometry.s2d.view;
#else
  view = scn->geometry.s3d.view;
#endif
  htable_bounce_clear(path);

  /* Discard faces with no emissivity */
  emissivity = scene_get_emissivity(scn, primitive_id, ispectral_band);
  if(emissivity <= 0.0)
    return RES_OK;

  /* Retrieve the sXd_scn primitive */
  sXd(scene_view_get_primitive)(view, (unsigned)primitive_id, &prim);
  /* Get the geometric normal of the input primitive */
  Xd(primitive_get_normal)(&prim, normal);
  /* Uniformly sample prim to define the origin of the radiative path */
  Xd(primitive_sample_position)(&prim, rng, pos);
  /* Cosine weighted sampling of the radiative path direction around `normal' */
  ssp_ran_hemisphere_cos_float(rng, normal, dir, NULL);

  transmissivity = 1.0;
  for(;;) { /* Here we go */
    struct sXd(primitive) prim_from = prim;
    range[0] = FLT_MIN, range[1] = FLT_MAX;

#if SGF_DIMENSIONALITY == 2
    s2d_scene_view_trace_ray_3d(view, pos, dir, range, &prim_from, &hit);
#else
    s3d_scene_view_trace_ray(view, pos, dir, range, &prim_from, &hit);
#endif

    /* Handle medium absorption */
    if(absorption_coef >= 0) {
      if(SXD(HIT_NONE)(&hit)) { /* The ray shoulnd't be outside the volume */
        log_error(dev,
"The radiative random walk goes to the infinity while the submitted geometry \n"
"should surround a close medium. This may be due to numerical issues or to an\n"
"invalid geometry definition.\n"
"Ray origin: %g, %g, %g\n",
          SPLIT3(pos));
        return RES_BAD_OP;
      } else {
        double weight, ka;

        /* Check the consistency of the medium description, i.e. the absorption
         * coefficient must be the same when it is fetched from any primitive
         * surrounding the current enclosure */
        ka = scene_get_absorption(scn, primitive_id, ispectral_band);
        if(ka != absorption_coef) {
          log_error(dev, "Inconsistent medium description.\n");
          return RES_BAD_ARG;
        }

        medium_transmissivity = exp(-absorption_coef*hit.distance);
        weight = transmissivity * (1-medium_transmissivity);
        transmissivity *= medium_transmissivity;
        medium_radiative_flux += weight;
      }
    } else if(SXD(HIT_NONE)(&hit)) { /* The ray is outside the volume */
      infinite_radiative_flux = transmissivity;
      break;
    }

    /* Retrieve the hit position and the hit primitive id */
    f3_mulf(vec0, dir, hit.distance);
    f3_add(pos, vec0, pos);
    prim = hit.prim;

    /* Fetch material property */
    emissivity = scene_get_emissivity(scn, prim.scene_prim_id, ispectral_band);
    specularity = scene_get_specularity(scn, prim.scene_prim_id, ispectral_band);
    reflectivity = scene_get_reflectivity(scn, prim.scene_prim_id, ispectral_band);
    if(transmissivity > trans_min) {
      const double weight = transmissivity * emissivity;
      double* pweight = htable_bounce_find(path, &prim.scene_prim_id);
      if(pweight) {
        *pweight += weight;
      } else {
        res = htable_bounce_set(path, &prim.scene_prim_id, &weight);
        if(res != RES_OK) return res;
      }
      transmissivity = transmissivity * (1.0 - emissivity);
#ifndef NDEBUG
      radiative_flux += weight;
#endif
    } else {
      /* Russian roulette */
      if(ssp_rng_canonical(rng) < emissivity) {
        double* pweight = htable_bounce_find(path, &prim.scene_prim_id);
        if(pweight) {
          *pweight += transmissivity;
        } else {
          res = htable_bounce_set(path, &prim.scene_prim_id, &transmissivity);
          if(res != RES_OK) return res;
        }
#ifndef NDEBUG
        radiative_flux += transmissivity;
#endif
        break;
      }
    }

    if(reflectivity <= 0.0) break;

    proba_reflec_spec = specularity / reflectivity;
    Xd(hit_get_normal)(&hit, normal);

    if(ssp_rng_canonical(rng) >= proba_reflec_spec) { /* Diffuse reflection */
      ssp_ran_hemisphere_cos_float(rng, normal, dir, NULL);
      ASSERT(f3_dot(normal, dir) > 0);
    } else { /* Specular reflection */
      const float tmp = -2.f * f3_dot(dir, normal);
      f3_mulf(vec0, normal, tmp);
      f3_add(dir, dir, vec0);
      f3_normalize(dir, dir);
    }
  }

  /* Flush MC weights */
  htable_bounce_begin(path, &it);
  htable_bounce_end(path, &end);
  while(!htable_bounce_iterator_eq(&it, &end)) {
    const size_t iprim = *htable_bounce_iterator_key_get(&it);
    const double weight = *htable_bounce_iterator_data_get(&it);
    accum_weight(accums + iprim, weight);
    htable_bounce_iterator_next(&it);
  }
  if(medium_radiative_flux) {
    accum_weight(accum_medium, medium_radiative_flux);
  }
  if(infinite_radiative_flux) {
    accum_weight(accum_infinity, infinite_radiative_flux);
  }

#if !defined(NDEBUG)
  /* Check the energy conservation property */
  ASSERT(eq_eps
    (radiative_flux + infinite_radiative_flux + medium_radiative_flux,
     1.0, 1.e6));
#endif
  return RES_OK;
}

#undef sXd
#undef SXD
#undef Xd
#undef SGF_DIMENSIONALITY

#endif /* !SGF_DIMENSIONALITY */
