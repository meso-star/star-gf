/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SGF_H
#define SGF_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SGF_SHARED_BUILD) /* Build shared library */
  #define SGF_API extern EXPORT_SYM
#elif defined(SGF_STATIC) /* Use/build static library */
  #define SGF_API extern LOCAL_SYM
#else /* Use shared library */
  #define SGF_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the sgf function `Func'
 * returns an error. One should use this macro on sgf function calls for which
 * no explicit error checking is performed */
#ifndef NDEBUG
  #define SGF(Func) ASSERT(sgf_ ## Func == RES_OK)
#else
  #define SGF(Func) sgf_ ## Func
#endif

/* Forward declaration of external types */
struct logger;
struct mem_allocator;
struct ssp_rng;

struct sgf_scene_desc {
  void (*get_indices)(const unsigned itri, unsigned ids[], void* ctx);
  void (*get_position)(const unsigned ivert, float pos[], void* ctx);

  double (*get_emissivity)
    (const unsigned itri, const unsigned iband, void* ctx);
  double (*get_reflectivity)
    (const unsigned itri, const unsigned iband, void* ctx);
  double (*get_specularity)
    (const unsigned itri, const unsigned iband, void* ctx);
  double (*get_absorption) /* May be NULL <=> no medium */
    (const unsigned itri, const unsigned iband, void* ctx);

  void* context; /* User defined data */
  unsigned nprims; /* #primitives */
  unsigned nverts; /* #vertices */
  unsigned nbands; /* #spectral bands */
};
#define SGF_SCENE_DESC_NULL__ { 0 }
static const struct sgf_scene_desc SGF_SCENE_DESC_NULL =
  SGF_SCENE_DESC_NULL__;

/* Estimated Gebart Factor between 2 faces */
struct sgf_status {
  double E; /* Expected value */
  double V; /* Variance */
  double SE; /* Standard error */
  size_t nsteps; /* #realisations */
};

/* Opaque types */
struct sgf_device;
struct sgf_estimator;
struct sgf_scene;

/*******************************************************************************
 * Device API
 ******************************************************************************/
BEGIN_DECLS

SGF_API res_T
sgf_device_create
  (struct logger* logger, /* May be NULL <=> use default logger */
   struct mem_allocator* allocator, /* May be NULL <=> Use default allocator */
   const int verbose, /* Verbosity level */
   struct sgf_device** dev);

SGF_API res_T
sgf_device_ref_get
  (struct sgf_device* dev);

SGF_API res_T
sgf_device_ref_put
  (struct sgf_device* dev);

/*******************************************************************************
 * Scene API
 ******************************************************************************/
SGF_API res_T
sgf_scene_create
  (struct sgf_device* dev,
   struct sgf_scene** scn);

SGF_API res_T
sgf_scene_ref_get
  (struct sgf_scene* scn);

SGF_API res_T
sgf_scene_ref_put
  (struct sgf_scene* scn);

/* Return RES_BAD_OP if an integration process is currently active onto the
 * provided scene */
SGF_API res_T
sgf_scene_setup_3d
  (struct sgf_scene* scn,
   const struct sgf_scene_desc* desc);

/* Return RES_BAD_OP if an integration process is currently active onto the
 * provided scene */
SGF_API res_T
sgf_scene_setup_2d
  (struct sgf_scene* scn,
   const struct sgf_scene_desc* desc);

SGF_API res_T
sgf_scene_primitives_count
  (struct sgf_scene* scn,
   unsigned* nprims);

SGF_API res_T
sgf_scene_begin_integration
  (struct sgf_scene* scn);

SGF_API res_T
sgf_scene_end_integration
  (struct sgf_scene* scn);

/*******************************************************************************
 * Integration API
 ******************************************************************************/
/* Numerical estimation of the Gebhart Factor between `primitive_id' and the
 * other scene primitives. The `sgf_scene_begin_integration' function hat to be
 * invoked onto the provided scene. RES_BAD_OP is returned if not. This
 * function is thread safe and can be invoked by several threads. */
SGF_API res_T
sgf_integrate
  (struct sgf_scene* scn,
   const size_t primitive_id,
   struct ssp_rng* rng,
   const size_t steps_count, /* #realisations */
   struct sgf_estimator** out_estimator);

SGF_API res_T
sgf_estimator_ref_get
  (struct sgf_estimator* estimator);

SGF_API res_T
sgf_estimator_ref_put
  (struct sgf_estimator* estimator);

/* Return the estimated Gebhart Factor between the estimator primitive and
 * `primitive_id'. */
SGF_API res_T
sgf_estimator_get_status
  (struct sgf_estimator* estimator,
   const size_t primitive_id,
   const size_t spectral_band_id,
   struct sgf_status* status);

/* Return the estimated radiative flux that hit nothing */
SGF_API res_T
sgf_estimator_get_status_infinity
  (struct sgf_estimator* estimator,
   const size_t spectral_band,
   struct sgf_status* status);

/* Return the estimated radiative flux absorbed by the medium */
SGF_API res_T
sgf_estimator_get_status_medium
  (struct sgf_estimator* estimator,
   const size_t spectral_band,
   struct sgf_status* status);

END_DECLS

#endif /* SGF_H */

