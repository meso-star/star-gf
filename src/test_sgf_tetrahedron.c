/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "test_sgf_utils.h"

#include <rsys/logger.h>
#include <rsys/stretchy_array.h>

#include <star/ssp.h>

#include <omp.h>

#define NSTEPS 100000L

static const float vertices[] = {
  0.57735026918962576450f, 0.f, 0.f,
  -0.28867513459481288225f, 0.5f, 0.f,
  -0.28867513459481288225f, -0.5f, 0.f,
  0.f, 0.f, 0.81649658092772603273f
};
static const size_t nvertices = sizeof(vertices) / (3*sizeof(float));

/* Front faces are CW. The normals point into the cube */
static const unsigned indices[] = {
  0, 1, 3,
  0, 2, 1,
  1, 2, 3,
  0, 3, 2
};
static const size_t nprims = sizeof(indices) / (3*sizeof(unsigned));

static const double emissivity[] = { 0.5, 1.0, 1.0, 1.0 };
static const double specularity[] = {  0.0, 0.0, 0.0, 0.0 };

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct shape_context shape;
  struct sgf_device* sgf = NULL;
  struct sgf_scene_desc desc = SGF_SCENE_DESC_NULL;
  struct sgf_scene* scn = NULL;
  struct sgf_status* status = NULL;
  struct ssp_rng_proxy* proxy = NULL;
  struct ssp_rng** rngs = NULL;
  size_t i;
  size_t nbuckets;
  int iprim;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);
  nbuckets = (unsigned)omp_get_num_procs();
  CHK(ssp_rng_proxy_create(&allocator, SSP_RNG_THREEFRY, nbuckets, &proxy) == RES_OK);
  CHK(sgf_device_create(NULL, &allocator, 1, &sgf) == RES_OK);
  CHK(sgf_scene_create(sgf, &scn) == RES_OK);

  shape.vertices = vertices;
  shape.nvertices = nvertices;
  shape.indices = indices;
  shape.nprimitives = nprims;
  shape.emissivity = emissivity;
  shape.specularity = specularity;
  shape.dim = 3;

  desc.get_position = get_position;
  desc.get_indices = get_indices;
  desc.get_emissivity = get_emissivity;
  desc.get_specularity = get_specularity;
  desc.get_reflectivity = get_reflectivity;
  desc.nprims = (unsigned)nprims;
  desc.nverts = (unsigned)nvertices;
  desc.nbands = 1;
  desc.context = &shape;

  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);

  status = sa_add(status, nprims*nprims);
  rngs = sa_add(rngs, nbuckets);

  FOR_EACH(i, 0, nbuckets)
    CHK(ssp_rng_proxy_create_rng(proxy, i, rngs + i) == RES_OK);

  CHK(sgf_scene_begin_integration(scn) == RES_OK);

  /* Gebhart Factor integration */
  #pragma omp parallel for
  for(iprim = 0; iprim < (int)nprims; ++iprim) {
    struct sgf_status* row = status + (size_t)iprim*nprims;
    struct sgf_estimator* estimator = NULL;
    struct sgf_status infinity;
    size_t iprim2;
    const int ithread = omp_get_thread_num();

    CHK(sgf_integrate
      (scn, (size_t)iprim, rngs[ithread], NSTEPS, &estimator) == RES_OK);

    FOR_EACH(iprim2, 0, nprims) {
      CHK(sgf_estimator_get_status(estimator, iprim2, 0, row + iprim2) == RES_OK);
      CHK(row[iprim2].nsteps == NSTEPS);
    }

    CHK(sgf_estimator_get_status_infinity(estimator, 0, &infinity) == RES_OK);
    CHK(eq_eps(infinity.E, 0, infinity.SE) == 1);

    CHK(sgf_estimator_ref_put(estimator) == RES_OK);
  }
  CHK(sgf_scene_end_integration(scn) == RES_OK);

  /* Expected value */
  for(iprim=0; iprim < (int)nprims; ++iprim) {
    const struct sgf_status* row = status + (size_t)iprim * nprims;
    unsigned icol;
    double sum = 0.0;
    FOR_EACH(icol, 0, nprims) {
      sum += row[icol].E;
      printf("%.6f  ", row[icol].E);
    }
    printf("\n");
    /* Ensure the energy conservation property */
    CHK(eq_eps(sum, 1.0, 1.e-3) == 1);
  }
  printf("\n");

  /* Standard error */
  for(iprim=0; iprim < (int)nprims; ++iprim) {
    const struct sgf_status* row = status + (size_t)iprim * nprims;
    unsigned icol;
    FOR_EACH(icol, 0, nprims) {
      printf("%.6f  ", row[icol].SE);
    }
    printf("\n");
  }

  CHK(ssp_rng_proxy_ref_put(proxy) == RES_OK);
  CHK(sgf_device_ref_put(sgf) == RES_OK);
  CHK(sgf_scene_ref_put(scn) == RES_OK);
  FOR_EACH(i, 0, nbuckets)
    CHK(ssp_rng_ref_put(rngs[i]) == RES_OK);

  sa_release(rngs);
  sa_release(status);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

