/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "test_sgf_utils.h"

#include <rsys/logger.h>
#include <rsys/stretchy_array.h>

#include <star/ssp.h>

#include <omp.h>

#define NSTEPS 10000L

/*
 * Analytic Gebhart Factor matrix
 *
 * 0.065871765 0.245053213 0.281090450 0.210375872 0.0838339939 0.113774706
 * 0.183789910 0.100632183 0.291901621 0.218467251 0.0870583783 0.118150656
 * 0.187393634 0.259468108 0.121154594 0.222750923 0.0887654054 0.120467336
 * 0.180322176 0.249676859 0.286394044 0.082269756 0.0854157674 0.115921399
 * 0.167667988 0.232155676 0.266296216 0.199303457 0.0267900995 0.107786564
 * 0.170662059 0.236301313 0.271051506 0.202862448 0.0808399227 0.038282752
 */

static const float vertices[] = {
  0.f, 0.f, 0.f,
  1.f, 0.f, 0.f,
  0.f, 1.f, 0.f,
  1.f, 1.f, 0.f,
  0.f, 0.f, 1.f,
  1.f, 0.f, 1.f,
  0.f, 1.f, 1.f,
  1.f, 1.f, 1.f
};
static const size_t nvertices = sizeof(vertices) / (3*sizeof(float));

/* Front faces are CW. The normals point into the cube */
static const unsigned indices[] = {
  0, 2, 1, 1, 2, 3, /* Front */
  0, 4, 2, 2, 4, 6, /* Left */
  4, 5, 6, 6, 5, 7, /* Back */
  3, 7, 1, 1, 7, 5, /* Right */
  2, 6, 3, 3, 6, 7, /* Top */
  0, 1, 4, 4, 1, 5 /* Bottom */
};
static const size_t nprims = sizeof(indices) / (3*sizeof(unsigned));

static const double emissivity[] = {
  0.6, 0.6, /* Front */
  0.8, 0.8, /* Left */
  0.9, 0.9, /* Back */
  0.7, 0.7, /* Right */
  0.3, 0.3, /* Top */
  0.4, 0.4 /* Bottom */
};

/* Emissivity used to simulate 2 infinite planes */
static const double emissivity_inf_bottom_top[] = {
  0, 0, /* Front */
  0, 0, /* Left */
  0, 0, /* Back */
  0, 0, /* Right */
  1, 1, /* Top */
  1, 1 /* Bottom */
};

static const double specularity[] = {
  0.0, 0.0, /* Front */
  0.0, 0.0, /* Left */
  0.0, 0.0, /* Back */
  0.0, 0.0, /* Right */
  0.0, 0.0, /* Top */
  0.0, 0.0 /* Bottom */
};

/* Specularity used to simulate 2 infinite planes */
static const double specularity_inf_bottom_top[] = {
  1.0, 1.0, /* Front */
  1.0, 1.0, /* Left */
  1.0, 1.0, /* Back */
  1.0, 1.0, /* Right */
  0.0, 0.0, /* Top */
  0.0, 0.0 /* Bottom */
};

/* Check the estimation of the bottom/top & bottom/medium Gebhart factors */
static void
check_bottom_top_medium_gf
  (struct sgf_scene* scn,
   struct ssp_rng* rng,
   const double gf_bottom_top, /* Ref of the bottom/top Gebhart factor */
   const double gf_bottom_medium) /* Ref of the bottom/medium Gebhart Factor */
{
  /* Indices of the top/bottom primitives */
  const size_t TOP0 = 8;
  const size_t TOP1 = 9;
  const size_t BOTTOM0 = 10;
  const size_t BOTTOM1 = 11;

  struct sgf_estimator* estimator;
  struct sgf_status status[6];
  double E, SE;

  CHK(sgf_scene_begin_integration(scn) == RES_OK);

  /* Estimate the Gebhart factors for the 1st triangle of the bottom face */
  CHK(sgf_integrate(scn, BOTTOM0, rng, NSTEPS, &estimator) == RES_OK);
  CHK(sgf_estimator_get_status(estimator, TOP0, 0, status + 0) == RES_OK);
  CHK(sgf_estimator_get_status(estimator, TOP1, 0, status + 1) == RES_OK);
  CHK(sgf_estimator_get_status_medium(estimator, 0, status + 2) == RES_OK);
  CHK(sgf_estimator_get_status_medium(estimator, 0, status + 3) == RES_OK);
  CHK(sgf_estimator_ref_put(estimator) == RES_OK);

  /* Estimate the Gebhart factors for the 2nd triangle of the bottom face */
  CHK(sgf_integrate(scn, BOTTOM1, rng, NSTEPS, &estimator) == RES_OK);
  CHK(sgf_estimator_get_status(estimator, TOP0, 0, status + 4) == RES_OK);
  CHK(sgf_estimator_get_status(estimator, TOP1, 0, status + 5) == RES_OK);
  CHK(sgf_estimator_ref_put(estimator) == RES_OK);

  /* Check the Gebhart factor between the bottom and the top plane.  */
  E = (status[0].E + status[1].E + status[4].E + status[5].E)/2;
  SE = (status[0].SE + status[1].SE + status[4].SE + status[5].SE)/2;
  CHK(eq_eps(E, gf_bottom_top, SE) == 1);

  /* Check the Gebhart factor between the bottom plane and the medium */
  E = (status[2].E + status[3].E)/2;
  SE = (status[2].SE + status[3].SE)/2;
  CHK(eq_eps(E, gf_bottom_medium, SE*3) == 1);

  CHK(sgf_scene_end_integration(scn) == RES_OK);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct shape_context shape;
  struct sgf_scene* scn;
  struct sgf_scene_desc desc = SGF_SCENE_DESC_NULL;
  struct sgf_device* sgf = NULL;
  struct sgf_status* status = NULL;
  struct ssp_rng_proxy* proxy;
  struct ssp_rng** rngs = NULL;
  double ka[12];
  int iprim;
  unsigned i;
  unsigned nbuckets;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);
  nbuckets = (unsigned)omp_get_num_procs();

  CHK(ssp_rng_proxy_create(&allocator, SSP_RNG_THREEFRY, nbuckets, &proxy) == RES_OK);
  CHK(sgf_device_create(NULL, &allocator, 1, &sgf) == RES_OK);
  CHK(sgf_scene_create(sgf, &scn) == RES_OK);

  shape.vertices = vertices;
  shape.nvertices = nvertices;
  shape.indices = indices;
  shape.nprimitives = nprims;
  shape.emissivity = emissivity;
  shape.specularity = specularity;
  shape.dim = 3;

  desc.get_position = get_position;
  desc.get_indices = get_indices;
  desc.get_emissivity = get_emissivity;
  desc.get_specularity = get_specularity;
  desc.get_reflectivity = get_reflectivity;
  desc.nprims = (unsigned)nprims;
  desc.nverts = (unsigned)nvertices;
  desc.nbands = 1;
  desc.context = &shape;

  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);

  status = sa_add(status, nprims*nprims);
  rngs = sa_add(rngs, nbuckets);

  FOR_EACH(i, 0, nbuckets)
    CHK(ssp_rng_proxy_create_rng(proxy, i, rngs + i) == RES_OK);

  CHK(sgf_scene_begin_integration(scn) == RES_OK);

  /* Integrate the Gebhart Factors */
  #pragma omp parallel for
  for(iprim = 0; iprim < (int)nprims; ++iprim) {
    size_t iprim2;
    struct sgf_status* row = status + (size_t)iprim * nprims;
    struct sgf_estimator* est = NULL;
    struct sgf_status infinity;
    const int ithread = omp_get_thread_num();

    CHK(sgf_integrate
      (scn, (size_t)iprim, rngs[ithread], NSTEPS, &est) == RES_OK);

    FOR_EACH(iprim2, 0, nprims) {
      CHK(sgf_estimator_get_status(est, iprim2, 0, row + iprim2) == RES_OK);
      CHK(row[iprim2].nsteps == NSTEPS);
    }

    CHK(sgf_estimator_get_status_infinity(est, 0, &infinity) == RES_OK);
    CHK(eq_eps(infinity.E, 0, infinity.SE) == 1);

    CHK(sgf_estimator_ref_put(est) == RES_OK);
  }

  /* Merge the radiative flux of coplanar primitives */
  for(iprim=0; iprim < (int)(nprims/2); ++iprim) {
    const struct sgf_status* row_src0 = status + (size_t)iprim * 2 * nprims;
    const struct sgf_status* row_src1 = row_src0 + nprims;
    size_t icol;
    double sum = 0;

    FOR_EACH(icol, 0, nprims/2) {
      const struct sgf_status* src0 = row_src0 + icol * 2;
      const struct sgf_status* src1 = src0 + 1;
      const struct sgf_status* src2 = row_src1 + icol * 2;
      const struct sgf_status* src3 = src2 + 1;
      double E = (src0->E + src1->E + src2->E + src3->E) / 2;

      sum += E;
      printf("%.6f  ", E);
    }
    printf("\n");
    CHK(eq_eps(sum, 1.0, 1.e-3) == 1); /* Ensure the energy conservation */
  }

  CHK(sgf_scene_end_integration(scn) == RES_OK);

  /*
   * Check medium attenuation with 2 parallel infinite planes. To simulate
   * this configuration, the top and bottom faces of the cube are fully
   * emissive.  The other ones are fully specular and have no emissivity
   */

  shape.absorption = ka;
  shape.emissivity = emissivity_inf_bottom_top;
  shape.specularity = specularity_inf_bottom_top;
  desc.get_absorption = get_absorption;

  FOR_EACH(i, 0, 12) ka[i] = 0;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rngs[0], 1, 0);

  FOR_EACH(i, 0, 12) ka[i] = 0.1;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rngs[0], 0.832583, 0.167417);

  FOR_EACH(i, 0, 12) ka[i] = 1;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rngs[0], 0.219384, 0.780616);

  FOR_EACH(i, 0, 12) ka[i] = 10;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rngs[0], 7.0975e-6, 0.999992902);

  CHK(ssp_rng_proxy_ref_put(proxy) == RES_OK);
  CHK(sgf_device_ref_put(sgf) == RES_OK);
  CHK(sgf_scene_ref_put(scn) == RES_OK);
  FOR_EACH(i, 0, nbuckets)
    CHK(ssp_rng_ref_put(rngs[i]) == RES_OK);
  sa_release(rngs);
  sa_release(status);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

