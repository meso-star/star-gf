/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "sgf_device_c.h"
#include "sgf_scene_c.h"

#include <star/s2d.h>
#include <star/s3d.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static int
hit_filter_s3d
  (const struct s3d_hit* hit,
   const float org[3],
   const float dir[3],
   const float range[2],
   void* ray_data,
   void* filter_data)
{
  struct s3d_primitive* prim_from = ray_data;
  (void)org, (void)dir, (void)range, (void)filter_data;

  if(!ray_data) return 0;
  /* Discard primitive from which the ray starts from */
  if(S3D_PRIMITIVE_EQ(prim_from, &hit->prim)) return 1;
  /* Ray starts on an edge and intersect the neighbor triangle */
  if(hit->distance <= 0) return 1;
  return 0;
}

static int
hit_filter_s2d
  (const struct s2d_hit* hit,
   const float org[3],
   const float dir[3],
   const float range[2],
   void* ray_data,
   void* filter_data)
{
  struct s2d_primitive* prim_from = ray_data;
  (void)org, (void)dir, (void)range, (void)filter_data;

  if(!ray_data) return 0;
  /* Discard primitive from which the ray starts from */
  if(S2D_PRIMITIVE_EQ(prim_from, &hit->prim)) return 1;
  /* Ray starts on an edge and intersect the neighbor triangle */
  if(hit->distance <= 0) return 1;
  return 0;
}

static FINLINE int
check_scene_desc(const struct sgf_scene_desc* desc)
{
  return desc
      && desc->get_indices
      && desc->get_emissivity
      && desc->get_reflectivity
      && desc->get_specularity
      && desc->get_position
      && desc->nprims
      && desc->nverts
      && desc->nbands;
}

static INLINE void
scene_release_s3d(struct sgf_scene* scn)
{
  if(scn->geometry.s3d.scn) {
    S3D(scene_ref_put(scn->geometry.s3d.scn));
    scn->geometry.s3d.scn = NULL;
  }
  if(scn->geometry.s3d.shape) {
    S3D(shape_ref_put(scn->geometry.s3d.shape));
    scn->geometry.s3d.shape = NULL;
  }
  scn->dimensionality = 0;
}

static INLINE res_T
scene_init_s3d(struct sgf_scene* scn)
{
  res_T res = RES_OK;
  ASSERT(scn);

  /* Create Star-3D data structures */
  res = s3d_scene_create(scn->dev->s3d, &scn->geometry.s3d.scn);
  if(res != RES_OK) goto error;
  res = s3d_shape_create_mesh(scn->dev->s3d, &scn->geometry.s3d.shape);
  if(res != RES_OK) goto error;
  res = s3d_mesh_set_hit_filter_function
    (scn->geometry.s3d.shape, &hit_filter_s3d, NULL);
  if(res != RES_OK) goto error;
  res = s3d_scene_attach_shape(scn->geometry.s3d.scn, scn->geometry.s3d.shape);
  if(res != RES_OK) goto error;
  scn->dimensionality = 3;

exit:
  return res;
error:
  scene_release_s3d(scn);
  goto exit;
}

static INLINE void
scene_release_s2d(struct sgf_scene* scn)
{
  if(scn->geometry.s2d.scn) {
    S2D(scene_ref_put(scn->geometry.s2d.scn));
    scn->geometry.s2d.scn = NULL;
  }
  if(scn->geometry.s2d.shape) {
    S2D(shape_ref_put(scn->geometry.s2d.shape));
    scn->geometry.s2d.shape = NULL;
  }
  scn->dimensionality = 0;
}

static INLINE res_T
scene_init_s2d(struct sgf_scene* scn)
{
  res_T res = RES_OK;
  ASSERT(scn);

  /* Create Star-3D data structures */
  res = s2d_scene_create(scn->dev->s2d, &scn->geometry.s2d.scn);
  if(res != RES_OK) goto error;
  res = s2d_shape_create_line_segments(scn->dev->s2d, &scn->geometry.s2d.shape);
  if(res != RES_OK) goto error;
  res = s2d_line_segments_set_hit_filter_function
    (scn->geometry.s2d.shape, &hit_filter_s2d, NULL);
  if(res != RES_OK) goto error;
  res = s2d_scene_attach_shape(scn->geometry.s2d.scn, scn->geometry.s2d.shape);
  if(res != RES_OK) goto error;
  scn->dimensionality = 2;

exit:
  return res;
error:
  scene_release_s2d(scn);
  goto exit;
}

static res_T
scene_setup
  (struct sgf_scene* scn,
   const struct sgf_scene_desc* desc,
   const int dimensionality,
   const char* caller)
{
  struct s2d_vertex_data vdata2d;
  struct s3d_vertex_data vdata3d;
  unsigned iprim, iband, i;
  double* abs, *emi, *refl, *spec;
  res_T res = RES_OK;

  if(!scn || !check_scene_desc(desc)) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Allocate the buffers of material attributes */
  #define RESIZE(V, Name) {                                                    \
    res = darray_double_resize(&(V), desc->nprims*desc->nbands);               \
    if(res != RES_OK) {                                                        \
      log_error(scn->dev, "%s: couldn't allocate the "Name" buffer.", caller); \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  RESIZE(scn->emi, "emissivity");
  RESIZE(scn->refl, "reflectivity");
  RESIZE(scn->spec, "specularity");
  if(desc->get_absorption) RESIZE(scn->abs, "absorption");
  #undef RESIZE

  /* Setup the material */
  abs = desc->get_absorption ? darray_double_data_get(&scn->abs) : NULL;
  emi = darray_double_data_get(&scn->emi);
  refl = darray_double_data_get(&scn->refl);
  spec = darray_double_data_get(&scn->spec);
  i = 0;
  FOR_EACH(iband, 0, desc->nbands) {
  FOR_EACH(iprim, 0, desc->nprims) {
    #define FETCH(Dst, Name, Low, Upp) {                                       \
      (Dst) = desc->get_##Name(iprim, iband, desc->context);                   \
      if((Dst) < (Low) || (Dst) > (Upp)) {                                     \
        log_error(scn->dev, "%s: invalid "STR(Name)" `%g'.\n", caller, (Dst)); \
        res = RES_BAD_ARG;                                                     \
        goto error;                                                            \
      }                                                                        \
    } (void) 0

    if(abs) FETCH(abs[i], absorption, 0, DBL_MAX);
    FETCH(emi[i], emissivity, 0, 1);
    FETCH(refl[i], reflectivity, 0, 1);
    FETCH(spec[i], specularity, 0, 1);
    #undef FETCH
    ++i;
  }}

  /* Initialise the Star-3D structures */
  if(scn->dimensionality != dimensionality) {
    if(scn->dimensionality == 2) scene_release_s2d(scn);
    if(scn->dimensionality == 3) scene_release_s3d(scn);

    if(dimensionality == 2) {
      res = scene_init_s2d(scn);
    } else {
      res = scene_init_s3d(scn);
    }
    if(res != RES_OK) goto error;
  }

  /* Setup the geometry */
  switch(scn->dimensionality) {
    case 2:
      vdata2d.usage = S2D_POSITION;
      vdata2d.type = S2D_FLOAT2;
      vdata2d.get = desc->get_position;
      s2d_line_segments_setup_indexed_vertices(scn->geometry.s2d.shape,
        desc->nprims, desc->get_indices, desc->nverts, &vdata2d, 1,
        desc->context);
      break;
    case 3:
      vdata3d.usage = S3D_POSITION;
      vdata3d.type = S3D_FLOAT3;
      vdata3d.get = desc->get_position;
      res = s3d_mesh_setup_indexed_vertices(scn->geometry.s3d.shape,
        desc->nprims, desc->get_indices, desc->nverts, &vdata3d, 1,
        desc->context);
      break;
    default: FATAL("Unreachable code\n"); break;
  }

  scn->nbands = desc->nbands;
  scn->nprims = desc->nprims;
  scn->has_medium = desc->get_absorption != NULL;

exit:
  return res;
error:
  if(scn) {
    darray_double_clear(&scn->abs);
    darray_double_clear(&scn->emi);
    darray_double_clear(&scn->refl);
    darray_double_clear(&scn->spec);
    scn->nprims = 0;
    scn->nbands = 0;
    scn->has_medium = 0;
  }
  goto exit;
}



static void
scene_release(ref_T* ref)
{
  struct sgf_device* dev;
  struct sgf_scene* scn;
  ASSERT(ref);
  scn = CONTAINER_OF(ref, struct sgf_scene, ref);
  dev = scn->dev;
  switch(scn->dimensionality) {
    case 2: scene_release_s2d(scn); break;
    case 3: scene_release_s3d(scn); break;
    default: FATAL("Unreachable code\n"); break;
  }
  darray_double_release(&scn->abs);
  darray_double_release(&scn->emi);
  darray_double_release(&scn->refl);
  darray_double_release(&scn->spec);
  MEM_RM(dev->allocator, scn);
  SGF(device_ref_put(dev));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sgf_scene_create(struct sgf_device* dev, struct sgf_scene** out_scn)
{
  struct sgf_scene* scn = NULL;
  res_T res = RES_OK;

  if(!dev || !out_scn) {
    res = RES_BAD_ARG;
    goto error;
  }

   scn = MEM_CALLOC(dev->allocator, 1, sizeof(struct sgf_scene));
   if(!scn) {
     res = RES_MEM_ERR;
     goto error;
   }
  ref_init(&scn->ref);
  SGF(device_ref_get(dev));
  scn->dev = dev;

  /* Initialise the buffers of material properties */
  darray_double_init(dev->allocator, &scn->abs);
  darray_double_init(dev->allocator, &scn->emi);
  darray_double_init(dev->allocator, &scn->refl);
  darray_double_init(dev->allocator, &scn->spec);

exit:
  if(out_scn) *out_scn = scn;
  return res;
error:
  if(scn) {
    SGF(scene_ref_put(scn));
    scn = NULL;
  }
  goto exit;
}

res_T
sgf_scene_ref_get(struct sgf_scene* scn)
{
  if(!scn) return RES_BAD_ARG;
  ref_get(&scn->ref);
  return RES_OK;
}

res_T
sgf_scene_ref_put(struct sgf_scene* scn)
{
  if(!scn) return RES_BAD_ARG;
  ref_put(&scn->ref, scene_release);
  return RES_OK;
}

res_T
sgf_scene_setup_3d(struct sgf_scene* scn, const struct sgf_scene_desc* desc)
{
  return scene_setup(scn, desc, 3, FUNC_NAME);
}
res_T
sgf_scene_setup_2d(struct sgf_scene* scn, const struct sgf_scene_desc* desc)
{
  return scene_setup(scn, desc, 2, FUNC_NAME);
}

res_T
sgf_scene_primitives_count(struct sgf_scene* scn, unsigned* nprims)
{
  if(!scn || !nprims) return RES_BAD_ARG;
  *nprims = scn->nprims;
  return RES_OK;
}

res_T
sgf_scene_begin_integration(struct sgf_scene* scn)
{
  int i;
  res_T res = RES_OK;
  if(!scn) return RES_BAD_ARG;

  if(scn->dimensionality == 2) {
    i = scn->geometry.s2d.view == NULL;
  } else {
    ASSERT(scn->dimensionality == 3);
    i = scn->geometry.s3d.view == NULL;
  }

  if(!i) {
    log_error(scn->dev, "%s: an integration process is already active.\n",
      FUNC_NAME);
    return RES_BAD_OP;
  }

  if(scn->dimensionality == 2) {
    res = s2d_scene_view_create(scn->geometry.s2d.scn,
      S2D_TRACE|S2D_GET_PRIMITIVE, &scn->geometry.s2d.view);
  } else {
    res = s3d_scene_view_create(scn->geometry.s3d.scn,
      S3D_TRACE|S3D_GET_PRIMITIVE, &scn->geometry.s3d.view);
  }
  return res;
}

res_T
sgf_scene_end_integration(struct sgf_scene* scn)
{
  res_T res = RES_OK;
  if(!scn) return RES_BAD_ARG;
  if(scn->dimensionality == 2) {
    if(!scn->geometry.s2d.view) { 
      res = RES_BAD_OP;
    } else {
      res = s2d_scene_view_ref_put(scn->geometry.s2d.view);
      if(res == RES_OK) scn->geometry.s2d.view = NULL;
    }
  } else {
    if(!scn->geometry.s3d.view) {
      res = RES_BAD_OP;
    } else {
      res = s3d_scene_view_ref_put(scn->geometry.s3d.view);
      if(res == RES_OK) scn->geometry.s3d.view = NULL;
    }
  }
  if(res != RES_OK) goto error;
exit:
  return res;
error:
  goto exit;
}

