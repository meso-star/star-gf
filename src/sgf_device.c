/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "sgf_device_c.h"

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>

#include <star/s2d.h>
#include <star/s3d.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
device_release(ref_T* ref)
{
  struct sgf_device* dev;
  ASSERT(ref);
  dev = CONTAINER_OF(ref, struct sgf_device, ref);
  if(dev->s2d) S2D(device_ref_put(dev->s2d));
  if(dev->s3d) S3D(device_ref_put(dev->s3d));
  MEM_RM(dev->allocator, dev);
}

/*******************************************************************************
 * API functions
 ******************************************************************************/
res_T
sgf_device_create
  (struct logger* log,
   struct mem_allocator* allocator,
   const int verbose,
   struct sgf_device** out_dev)
{
  struct mem_allocator* mem_allocator;
  struct sgf_device* dev = NULL;
  struct logger* logger = NULL;
  res_T res = RES_OK;

  if(!out_dev) {
    res = RES_BAD_ARG;
    goto error;
  }

  mem_allocator = allocator ? allocator : &mem_default_allocator;
  logger = log ? log : LOGGER_DEFAULT;

  dev = MEM_CALLOC(mem_allocator, 1, sizeof(struct sgf_device));
  if(!dev) {
    if(verbose) {
      logger_print(logger, LOG_ERROR,
        "%s: couldn't allocate the Star-Gebhart-Factor device.\n", FUNC_NAME);
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&dev->ref);
  dev->allocator = mem_allocator;
  dev->logger = logger;
  dev->verbose = verbose;

  res = s3d_device_create(NULL, dev->allocator, 0, &dev->s3d);
  if(res != RES_OK) {
    log_error(dev, "%s: couldn't create the Star-3D device.\n", FUNC_NAME);
    goto error;
  }
  res = s2d_device_create(NULL, dev->allocator, 0, &dev->s2d);
  if(res != RES_OK) {
    log_error(dev, "%s: couldn't create the Star-2D device.\n", FUNC_NAME);
    goto error;
  }

exit:
  if(out_dev) *out_dev = dev;
  return res;
error:
  if(dev) {
    SGF(device_ref_put(dev));
    dev = NULL;
  }
  goto exit;
}

res_T
sgf_device_ref_get(struct sgf_device* dev)
{
  if(!dev) return RES_BAD_ARG;
  ref_get(&dev->ref);
  return RES_OK;
}

res_T
sgf_device_ref_put(struct sgf_device* dev)
{
  if(!dev) return RES_BAD_ARG;
  ref_put(&dev->ref, device_release);
  return RES_OK;
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
void
log_error(struct sgf_device* dev, const char* msg, ...)
{
  va_list vargs;
  res_T res; (void)res;
  ASSERT(dev && msg);

  if(!dev->verbose) return;

  va_start(vargs, msg);
  res = logger_vprint(dev->logger, LOG_ERROR, msg, vargs);
  ASSERT(res == RES_OK);
  va_end(vargs);
}


void
log_warning(struct sgf_device* dev, const char* msg, ...)
{
  va_list vargs;
  res_T res; (void)res;
  ASSERT(dev && msg);

  if(!dev->verbose) return;

  va_start(vargs, msg);
  res = logger_vprint(dev->logger, LOG_WARNING, msg, vargs);
  ASSERT(res == RES_OK);
  va_end(vargs);
}
