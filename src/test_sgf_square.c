/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "test_sgf_utils.h"

#include <rsys/logger.h>
#include <star/s2d.h>
#include <star/ssp.h>

#include <limits.h>

#define NSTEPS 100000

static const float vertices[] = {
  1.f, 0.f,
  0.f, 0.f,
  0.f, 1.f,
  1.f, 1.f
};
const unsigned nverts = sizeof(vertices)/(2*sizeof(float));

const unsigned indices[] = {
  0, 1, /* Bottom */
  1, 2, /* Left */
  2, 3, /* Top */
  3, 0  /* Right */
};
const unsigned nsegs = sizeof(indices)/(2*sizeof(unsigned));

static const double emissivity[] = {
  1.0, /* Bottom */
  1.0, /* Left */
  1.0, /* Top */
  1.0  /* Right */
};

/* Emissivity used to simulate 2 infinite planes */
static const double emissivity_inf_bottom_top[] = {
  1, /* Bottom */
  0, /* Left */
  1, /* Top */
  0 /* Right */
};

static const double specularity[] = {
  0.0, /* Bottom */
  0.0, /* Left */
  0.0, /* Top */
  0.0  /* Right */
};

/* Specularity used to simulate 2 infinite segments */
static const double specularity_inf_bottom_top[] = {
  0.0, /* Bottom */
  1.0, /* Left */
  0.0, /* Top */
  1.0  /* Right */
};

/* Check the estimation of the bottom/top & bottom/medium Gebhart factors */
static void
check_bottom_top_medium_gf
  (struct sgf_scene* scn,
   struct ssp_rng* rng,
   const double gf_bottom_top, /* Ref of the bottom/top Gebhart factor */
   const double gf_bottom_medium) /* Ref of the bottom/medium Gebhart Factor */
{
  /* Indices of the top/bottom primitives */
  const size_t TOP = 2;
  const size_t BOTTOM = 0;

  struct sgf_estimator* estimator;
  struct sgf_status status[2];

  CHK(sgf_scene_begin_integration(scn) == RES_OK);

  /* Estimate the Gebhart factors for the bottom segment */
  CHK(sgf_integrate(scn, BOTTOM, rng, NSTEPS, &estimator) == RES_OK);
  CHK(sgf_estimator_get_status(estimator, TOP, 0, status + 0) == RES_OK);
  CHK(sgf_estimator_get_status_medium(estimator, 0, status + 1) == RES_OK);
  CHK(sgf_estimator_ref_put(estimator) == RES_OK);

  /* Check the Gebhart factor between the bottom and the top plane.  */
  CHK(eq_eps(status[0].E, gf_bottom_top, status[0].SE) == 1);

  /* Check the Gebhart factor between the bottom plane and the medium */
  CHK(eq_eps(status[1].E, gf_bottom_medium, status[1].SE) == 1);

  CHK(sgf_scene_end_integration(scn) == RES_OK);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct shape_context shape;
  struct sgf_device* sgf;
  struct sgf_scene* scn;
  struct sgf_scene_desc desc = SGF_SCENE_DESC_NULL;
  struct sgf_status status;
  struct sgf_estimator* estimator;
  struct ssp_rng* rng;
  double ka[4];
  size_t iprim, i;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(ssp_rng_create(&allocator, SSP_RNG_THREEFRY, &rng) == RES_OK);
  CHK(sgf_device_create(NULL, &allocator, 1, &sgf) == RES_OK);
  CHK(sgf_scene_create(sgf, &scn) == RES_OK);

  shape.vertices = vertices;
  shape.nvertices = nverts;
  shape.indices = indices;
  shape.nprimitives = nsegs;
  shape.emissivity = emissivity;
  shape.specularity = specularity;
  shape.dim = 2;

  desc.get_position = get_position;
  desc.get_indices = get_indices;
  desc.get_emissivity = get_emissivity;
  desc.get_specularity = get_specularity;
  desc.get_reflectivity = get_reflectivity;
  desc.nprims = (unsigned)nsegs;
  desc.nverts = (unsigned)nverts;
  desc.nbands = 1;
  desc.context = &shape;

  CHK(sgf_scene_setup_2d(scn, &desc) == RES_OK);
  CHK(sgf_integrate(scn, 0, rng, NSTEPS, &estimator) == RES_BAD_OP);

  CHK(sgf_scene_begin_integration(scn) == RES_OK);

  FOR_EACH(iprim, 0, 3) {
    CHK(sgf_integrate(scn, iprim, rng, NSTEPS, &estimator) == RES_OK);
    FOR_EACH(i, 0, 3) {
      CHK(sgf_estimator_get_status(estimator, i, 0, &status) == RES_OK);

      if(i == iprim) {
        CHK(eq_eps(status.E, 0, status.SE) == 1);
      } else if(i == iprim + 2 || i == iprim - 2) { /* parallel faces */
        CHK(eq_eps(status.E, sqrt(2) - 1, 2*status.SE) == 1);
      } else { /* Orthogonal faces */
        CHK(iprim == i+1 || iprim == i-1);
        CHK(eq_eps(status.E, 1 - sqrt(2)/2, 2*status.SE) == 1);
      }
    }
    CHK(sgf_estimator_get_status_infinity(estimator, 0, &status) == RES_OK);
    CHK(eq_eps(status.E, 0, status.SE) == 1);

    CHK(sgf_estimator_ref_put(estimator) == RES_OK);
  }

  CHK(sgf_integrate(scn, 4, rng,  NSTEPS, &estimator) == RES_BAD_ARG);

  CHK(sgf_scene_end_integration(scn) == RES_OK);

  /*
   * Check medium attenuation with 2 parallel infinite segments. To simulate
   * this configuration, the top and bottom segments of the square are fully
   * emissive.  The other ones are fully specular and have no emissivity
   */

  shape.absorption = ka;
  shape.emissivity = emissivity_inf_bottom_top;
  shape.specularity = specularity_inf_bottom_top;
  desc.get_absorption = get_absorption;

  FOR_EACH(i, 0, 4) ka[i] = 0;
  CHK(sgf_scene_setup_2d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rng, 1, 0);

  FOR_EACH(i, 0, 4) ka[i] = 0.1;
  CHK(sgf_scene_setup_2d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rng, 0.832583, 0.167417);

  FOR_EACH(i, 0, 4) ka[i] = 1;
  CHK(sgf_scene_setup_2d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rng, 0.219384, 0.780616);

  FOR_EACH(i, 0, 4) ka[i] = 10;
  CHK(sgf_scene_setup_2d(scn, &desc) == RES_OK);
  check_bottom_top_medium_gf(scn, rng, 7.0975e-6, 0.999992902);

  CHK(ssp_rng_ref_put(rng) == RES_OK);
  CHK(sgf_device_ref_put(sgf) == RES_OK);
  CHK(sgf_scene_ref_put(scn) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

