/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafterf support */

#include "sgf.h"
#include "sgf_device_c.h"
#include "sgf_realisation.h"

#include <star/s3d.h>
#include <star/ssp.h>

#include <rsys/dynamic_array.h>
#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>

/* A random walk may fail du to numerical inaccuracy. The following constant
 * empirically defines the maximum number of "random walk" attempts before an
 * error occurs. */
#define MAX_FAILURES 10

/* Generate the accum dynamic array data type */
#define DARRAY_NAME accum
#define DARRAY_DATA struct accum
#include <rsys/dynamic_array.h>

/* Generate the 2D realisation function */
#define SGF_DIMENSIONALITY 2
#include "sgf_realisation.h"

/* Generate the 3D realisation function */
#define SGF_DIMENSIONALITY 3
#include "sgf_realisation.h"

/* Estimator of the Gebhart Factor of a given face to all the other ones */
struct sgf_estimator {
  /* Per spectral band & per primitive radiative flux */
  struct darray_accum buf;
  /* Per spectral band radiative flux going to the infinity */
  struct darray_accum buf_infinity;
  /* Per spectral band radiative flux absorbed by the medium */
  struct darray_accum buf_medium;

  size_t nsteps; /* # realisations */
  size_t nprims; /* # primitives */
  size_t nbands; /* # spectral bands */

  struct sgf_device* dev;
  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static FINLINE void
setup_status
  (const struct accum* acc,
   const size_t nsteps,
   struct sgf_status* status)
{
  ASSERT(acc && status && nsteps);

  status->E = acc->radiative_flux / (double)nsteps; /* Expected value */
  status->V = acc->sqr_radiative_flux / (double)nsteps - status->E*status->E;
  status->V = MMAX(status->V, 0);
  status->SE = sqrt(status->V / (double)nsteps); /* Standard error */
  status->nsteps = nsteps; /* # realisations */
}

static res_T
estimator_create(struct sgf_device* dev, struct sgf_estimator** out_estimator)
{
  struct sgf_estimator* estimator = NULL;
  res_T res = RES_OK;
  ASSERT(dev && out_estimator);

  estimator = MEM_CALLOC(dev->allocator, 1, sizeof(struct sgf_estimator));
  if(!estimator) {
    log_error(dev,
"Not enough memory space: couldn't allocate the Gebhart Factor estimator.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&estimator->ref);
  SGF(device_ref_get(dev));
  estimator->dev = dev;
  darray_accum_init(dev->allocator, &estimator->buf);
  darray_accum_init(dev->allocator, &estimator->buf_infinity);
  darray_accum_init(dev->allocator, &estimator->buf_medium);

exit:
  *out_estimator = estimator;
  return res;

error:
  if(estimator) {
    SGF(estimator_ref_put(estimator));
    estimator = NULL;
  }
  goto exit;
}

static void
estimator_release(ref_T* ref)
{
  struct sgf_device* dev;
  struct sgf_estimator* estimator;
  ASSERT(ref);
  estimator = CONTAINER_OF(ref, struct sgf_estimator, ref);
  darray_accum_release(&estimator->buf);
  darray_accum_release(&estimator->buf_infinity);
  darray_accum_release(&estimator->buf_medium);
  dev = estimator->dev;
  MEM_RM(dev->allocator, estimator);
  SGF(device_ref_put(dev));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sgf_integrate
  (struct sgf_scene* scn,
   const size_t iprim,
   struct ssp_rng* rng,
   const size_t steps_count,
   struct sgf_estimator** out_estimator)
{
  struct htable_bounce path;
  struct sgf_estimator* estimator = NULL;
  size_t istep;
  size_t iband;
  size_t nprims;
  void* view;
  res_T res = RES_OK;
  gebhart_radiative_path_T gebhart_radiative_path;

  if(!scn) return RES_BAD_ARG;
  htable_bounce_init(scn->dev->allocator, &path);

  if(!steps_count || !rng || !scn || !out_estimator) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = estimator_create(scn->dev, &estimator);
  if(res != RES_OK) goto error;

  switch(scn->dimensionality) {
    case 2:
      view = scn->geometry.s2d.view;
      gebhart_radiative_path = gebhart_radiative_path_2d;
      if(view) S2D(scene_view_primitives_count(view, &nprims));
      break;
    case 3:
      view = scn->geometry.s3d.view;
      gebhart_radiative_path = gebhart_radiative_path_3d;
      if(view) S3D(scene_view_primitives_count(view, &nprims));
      break;
    default: FATAL("Unreachable code\n"); break;
  }

  /* Check scene active sessions */
  if(!view) {
    log_error(scn->dev,
      "%s: no active integration on subimitted scene.\n", FUNC_NAME);
    res = RES_BAD_OP;
    goto error;
  }

  /* Check submitted primitive_id */
  if(iprim >= nprims) {
    log_error(scn->dev,  "%s: invalid primitive index `%lu'\n",
      FUNC_NAME, (unsigned long)iprim);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Allocate and init the accumulators of radiative flux */
  res = darray_accum_resize(&estimator->buf, nprims*scn->nbands);
  if(res != RES_OK) {
    log_error(scn->dev, "%s: couldn't allocate the Gebhart Factor result buffer.\n",
      FUNC_NAME);
    goto error;
  }
  memset(darray_accum_data_get(&estimator->buf), 0,
    darray_accum_size_get(&estimator->buf)*sizeof(struct accum));

  if(scn->has_medium) {
    /* Allocate and init the accumulators of absorbed radiative flux */
    res = darray_accum_resize(&estimator->buf_medium, scn->nbands);
    if(res != RES_OK) {
      log_error(scn->dev,
"%s: couldn't allocate the accumulators of the per spectral radiative flux "
"absorbed by the medium.\n", FUNC_NAME);
      goto error;
    }
    memset(darray_accum_data_get(&estimator->buf_medium), 0,
      darray_accum_size_get(&estimator->buf_medium)*sizeof(struct accum));
  } else {
    /* Allocate and init the accumulators of infinite radiative flux */
    res = darray_accum_resize(&estimator->buf_infinity, scn->nbands);
    if(res != RES_OK) {
      log_error(scn->dev,
"%s: couldn't allocate the accumulators of the per spectral band radiative flux"
" that goes to the infinite.\n", FUNC_NAME);
      goto error;
    }
    memset(darray_accum_data_get(&estimator->buf_infinity), 0,
      darray_accum_size_get(&estimator->buf_infinity)*sizeof(struct accum));
  }

  /* Invoke the Gebhart factor integration. */
  FOR_EACH(iband, 0, scn->nbands) {
    struct accum* accums = NULL;
    struct accum* accum_infinity = NULL;
    struct accum* accum_medium = NULL;
    double ka = -1; /* Absorption coefficient of the medium */

    accums = darray_accum_data_get(&estimator->buf) + iband * nprims;
    if(!scn->has_medium) {
      accum_infinity = darray_accum_data_get(&estimator->buf_infinity) + iband;
    } else {
      accum_medium = darray_accum_data_get(&estimator->buf_medium) + iband;
      ka = scene_get_absorption(scn, iprim, iband);
      if(ka < 0) {
        log_error(scn->dev, "%s: invalid absorption coefficient `%g'.\n",
          FUNC_NAME, ka);
        res = RES_BAD_ARG;
        goto error;
      }
    }

    FOR_EACH(istep, 0, steps_count) {
      size_t nfailures = 0;
      do {
        res = gebhart_radiative_path(scn->dev, accums, accum_infinity,
          accum_medium, rng, &path, ka, iband, iprim, scn);
        if(res == RES_BAD_OP) {
          log_error(scn->dev, "%s: reject radiative random walk.\n", FUNC_NAME);
          ++nfailures;
        } else if(res != RES_OK) {
          goto error;
        }
      } while(res != RES_OK && nfailures < MAX_FAILURES);

      if(++nfailures > MAX_FAILURES) {
        log_error(scn->dev, "%s: too many numerical issues.\n", FUNC_NAME);
        goto error;
      }
    }
  }
  estimator->nsteps = steps_count;
  estimator->nprims = nprims;
  estimator->nbands = scn->nbands;

exit:
  if(out_estimator) *out_estimator = estimator;
  htable_bounce_release(&path);
  return res;
error:
  if(estimator) SGF(estimator_ref_put(estimator));
  goto exit;
}

res_T
sgf_estimator_ref_get(struct sgf_estimator* estimator)
{
  if(!estimator) return RES_BAD_ARG;
  ref_get(&estimator->ref);
  return RES_OK;
}

res_T
sgf_estimator_ref_put(struct sgf_estimator* estimator)
{
  if(!estimator) return RES_BAD_ARG;
  ref_put(&estimator->ref, estimator_release);
  return RES_OK;
}

res_T
sgf_estimator_get_status
  (struct sgf_estimator* estimator,
   const size_t iprim,
   const size_t iband,
   struct sgf_status* status)
{
  const struct accum* acc;
  size_t iacc;

  if(!estimator || !status)
    return RES_BAD_ARG;

  if(iprim >= estimator->nprims) {
    log_error(estimator->dev, "%s: out of bound primitive index `%lu'.\n",
      FUNC_NAME, (unsigned long)iprim);
    return RES_BAD_ARG;
  }

  if(iband >= estimator->nbands) {
    log_error(estimator->dev, "%s: out of bound spectral band index `%lu'.\n",
      FUNC_NAME, (unsigned long)iband);
    return RES_BAD_ARG;
  }

  iacc = iband * estimator->nprims + iprim;
  acc = darray_accum_cdata_get(&estimator->buf) + iacc;
  setup_status(acc, estimator->nsteps, status);
  return RES_OK;
}

res_T
sgf_estimator_get_status_infinity
  (struct sgf_estimator* estimator,
   const size_t iband,
   struct sgf_status* status)
{
  const struct accum* acc;

  if(!estimator || !status)
    return RES_BAD_ARG;

  if(iband >= estimator->nbands) {
    log_error(estimator->dev, "%s: out of bound spectral band index `%lu'.\n",
      FUNC_NAME, (unsigned long)iband);
    return RES_BAD_ARG;
  }
  if(darray_accum_size_get(&estimator->buf_infinity) != 0) {
    acc = darray_accum_cdata_get(&estimator->buf_infinity) + iband;
    setup_status(acc, estimator->nsteps, status);
  } else {
    status->E = status->V = status->SE = 0;
    status->nsteps = estimator->nsteps;
  }
  return RES_OK;
}

res_T
sgf_estimator_get_status_medium
  (struct sgf_estimator* estimator,
   const size_t iband,
   struct sgf_status* status)
{
  const struct accum* acc;

  if(!estimator || !status)
    return RES_BAD_ARG;

  if(iband >= estimator->nbands) {
    log_error(estimator->dev, "%s: out of bound spectral band index `%lu'\n.",
      FUNC_NAME, (unsigned long)iband);
    return RES_BAD_ARG;
  }
  if(darray_accum_size_get(&estimator->buf_medium) != 0) {
    acc = darray_accum_cdata_get(&estimator->buf_medium) + iband;
    setup_status(acc, estimator->nsteps, status);
  } else {
    status->E = status->V = status->SE = 0;
    status->nsteps = estimator->nsteps;
  }
  return RES_OK;
}

