/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "test_sgf_utils.h"

#include <rsys/stretchy_array.h>

#include <star/s3d.h>
#include <star/ssp.h>

#define NSTEPS 10000L

static const float vertices[] = {
  0.f, 0.f, 0.f,
  1.f, 0.f, 0.f,
  0.f, 1.f, 0.f,
  1.f, 1.f, 0.f,
  0.f, 0.f, 1.f,
  1.f, 0.f, 1.f,
  0.f, 1.f, 1.f,
  1.f, 1.f, 1.f
};
static const size_t nvertices = sizeof(vertices) / (3*sizeof(float));

/* Front faces are CW. The normals point into the cube */
static const unsigned indices[] = {
  0, 2, 1, 1, 2, 3, /* Front */
  0, 4, 2, 2, 4, 6, /* Left */
  4, 5, 6, 6, 5, 7, /* Back */
  3, 7, 1, 1, 7, 5, /* Right */
  2, 6, 3, 3, 6, 7, /* Top */
  0, 1, 4, 4, 1, 5 /* Bottom */
};
static const size_t nprims = sizeof(indices) / (3*sizeof(unsigned));

static const double emissivity[] = {
  0.6, 0.6, /* Front */
  0.8, 0.8, /* Left */
  0.9, 0.9, /* Back */
  0.7, 0.7, /* Right */
  0.3, 0.3, /* Top */
  0.4, 0.4 /* Bottom */
};

static const double specularity[] = {
  0.0, 0.0, /* Front */
  0.0, 0.0, /* Left */
  0.0, 0.0, /* Back */
  0.0, 0.0, /* Right */
  0.0, 0.0, /* Top */
  0.0, 0.0 /* Bottom */
};

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct shape_context shape;
  struct sgf_device* sgf = NULL;
  struct sgf_estimator* estimator = NULL;
  struct sgf_scene_desc desc = SGF_SCENE_DESC_NULL;
  struct sgf_scene* scn;
  struct sgf_status status;
  struct ssp_rng* rng = NULL;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(ssp_rng_create(&allocator, SSP_RNG_THREEFRY, &rng) == RES_OK);
  CHK(sgf_device_create(NULL, &allocator, 1, &sgf) == RES_OK);
  CHK(sgf_scene_create(sgf, &scn) == RES_OK);

  shape.vertices = vertices;
  shape.nvertices = nvertices;
  shape.indices = indices;
  shape.nprimitives = nprims;
  shape.emissivity = emissivity;
  shape.specularity = specularity;
  shape.dim = 3;

  desc.get_position = get_position;
  desc.get_indices = get_indices;
  desc.get_emissivity = get_emissivity;
  desc.get_specularity = get_specularity;
  desc.get_reflectivity = get_reflectivity;
  desc.nprims = (unsigned)nprims;
  desc.nverts = (unsigned)nvertices;
  desc.nbands = 1;
  desc.context = &shape;

  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);
  CHK(sgf_scene_begin_integration(scn) == RES_OK);

  CHK(sgf_integrate(NULL, SIZE_MAX, NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, rng, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, rng, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, rng, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, rng, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, NULL, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, NULL, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, NULL, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, NULL, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, rng, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, rng, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, rng, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, rng, NSTEPS, NULL) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, NULL, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, NULL, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, NULL, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, NULL, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, rng, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, rng, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, rng, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, rng, 0, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, NULL, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, NULL, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, NULL, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, NULL, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, SIZE_MAX, rng, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, SIZE_MAX, rng, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(NULL, 0, rng, NSTEPS, &estimator) == RES_BAD_ARG);
  CHK(sgf_integrate(scn, 0, rng, NSTEPS, &estimator) == RES_OK);

  CHK(sgf_estimator_get_status(NULL, SIZE_MAX, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, SIZE_MAX, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, 0, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, 0, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, SIZE_MAX, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, SIZE_MAX, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, 0, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, 0, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, SIZE_MAX, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, SIZE_MAX, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, 0, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, 0, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, SIZE_MAX, 0, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, SIZE_MAX, 0, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(NULL, 0, 0, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status(estimator, 0, 0, &status) == RES_OK);
  CHK(status.nsteps == NSTEPS);

  CHK(sgf_estimator_get_status_infinity(NULL, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(estimator, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(estimator, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(NULL, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(estimator, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(NULL, 0, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_infinity(estimator, 0, &status) == RES_OK);
  CHK(eq_eps(status.E, 0, status.SE) == 1);
  CHK(status.nsteps == NSTEPS);

  CHK(sgf_estimator_get_status_medium(NULL, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(estimator, SIZE_MAX, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(estimator, 0, NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(NULL, SIZE_MAX, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(estimator, SIZE_MAX,&status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(NULL, 0, &status) == RES_BAD_ARG);
  CHK(sgf_estimator_get_status_medium(estimator, 0, &status) == RES_OK);
  CHK(status.E == 0);
  CHK(status.V == 0);
  CHK(status.SE == 0);
  CHK(status.nsteps == NSTEPS);

  CHK(sgf_estimator_ref_get(NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_ref_get(estimator) == RES_OK);
  CHK(sgf_estimator_ref_put(NULL) == RES_BAD_ARG);
  CHK(sgf_estimator_ref_put(estimator) == RES_OK);
  CHK(sgf_estimator_ref_put(estimator) == RES_OK);

  CHK(sgf_scene_end_integration(scn) == RES_OK);
  CHK(sgf_integrate(scn, 0, rng, NSTEPS, &estimator) == RES_BAD_OP);

  CHK(ssp_rng_ref_put(rng) == RES_OK);
  CHK(sgf_device_ref_put(sgf) == RES_OK);
  CHK(sgf_scene_ref_put(scn) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

