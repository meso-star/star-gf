/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_SGF_UTILS_H
#define TEST_SGF_UTILS_H

#include <rsys/mem_allocator.h>
#include <stdio.h>

struct shape_context {
  const float* vertices;
  size_t nvertices;
  const unsigned* indices;
  size_t nprimitives;
  const double* emissivity;
  const double* specularity;
  const double* absorption;
  unsigned dim;
};

static INLINE void
get_indices(const unsigned itri, unsigned ids[], void* data)
{
  const struct shape_context* shape = data;
  const unsigned id = itri * shape->dim;
  unsigned i;
  CHK(shape != NULL);
  CHK(ids != NULL);
  CHK(shape->dim == 2 || shape->dim == 3);
  CHK(itri < shape->nprimitives);
  FOR_EACH(i, 0, shape->dim) ids[i] = shape->indices[id + i];
}

static INLINE void
get_position(const unsigned ivert, float pos[], void* data)
{
  const struct shape_context* shape = data;
  const unsigned id = ivert*shape->dim;
  unsigned i;
  CHK(shape != NULL);
  CHK(pos != NULL);
  CHK(ivert < shape->nvertices);
  FOR_EACH(i, 0, shape->dim) pos[i] = shape->vertices[id + i];
}

static INLINE double
get_absorption(const unsigned iprim, const unsigned iband, void* ctx)
{
  struct shape_context* shape = ctx;
  CHK(shape != NULL);
  (void)iband;
  return shape->absorption[iprim];
}

static INLINE double
get_emissivity(const unsigned iprim, const unsigned iband, void* ctx)
{
  struct shape_context* shape = ctx;
  CHK(shape != NULL);
  (void)iband;
  return shape->emissivity[iprim];
}

static INLINE double
get_specularity(const unsigned iprim, const unsigned iband, void* ctx)
{
  struct shape_context* shape = ctx;
  CHK(shape != NULL);
  (void)iband;
  return shape->specularity[iprim];
}

static INLINE double
get_reflectivity(const unsigned iprim, const unsigned iband, void* ctx)
{
  return 1 - get_emissivity(iprim, iband, ctx);
}

static INLINE void
dump_triangle_mesh(struct shape_context* mesh)
{
  unsigned i;
  CHK(mesh != NULL);
  FOR_EACH(i, 0, mesh->nvertices)
    printf("v %f %f %f\n", SPLIT3(mesh->vertices + i*3));
  FOR_EACH(i, 0, mesh->nprimitives) {
    printf("f %d %d %d\n",
      mesh->indices[i*3 + 0] + 1,
      mesh->indices[i*3 + 1] + 1,
      mesh->indices[i*3 + 2] + 1);
  }
}

static void
check_memory_allocator(struct mem_allocator* allocator)
{
  if(MEM_ALLOCATED_SIZE(allocator)) {
    char dump[512];
    MEM_DUMP(allocator, dump, sizeof(dump)/sizeof(char));
    fprintf(stderr, "%s\n", dump);
    FATAL("Memory leaks\n");
  }
}

#endif /* TEST_SGF_UTILS_H */

