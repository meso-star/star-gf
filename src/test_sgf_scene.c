/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sgf.h"
#include "test_sgf_utils.h"

/*******************************************************************************
 * Plane data
 ******************************************************************************/
static const float plane_verts[] = {
  0.f, 0.f, 0.f,
  1.f, 0.f, 0.f,
  0.f, 1.f, 0.f,
  1.f, 1.f, 0.f,
};
static const size_t plane_nverts = sizeof(plane_verts) / (3*sizeof(float));
static const unsigned plane_ids[] = { 0, 2, 1, 1, 2, 3 };
static const size_t plane_nprims = sizeof(plane_ids) / (3*sizeof(unsigned));
static const double plane_emi[] = { 0.6, 0.6 };
static const double plane_emi_bad[] = { 0.6, 1.1 };
static const double plane_spec[] = { 0.0, 0.0 };
static const double plane_spec_bad[] = { 1.1, 0.0 };
static const double plane_abs[] = { 0.0, 0.0 };
static const double plane_abs_bad[] = { -0.1, 0.0 };

/*******************************************************************************
 * Square data
 ******************************************************************************/
static const float square_verts[] = {
  1.f, 0.f,
  0.f, 0.f,
  0.f, 1.f,
  1.f, 1.f
};
const unsigned square_nverts = sizeof(square_verts)/(2*sizeof(float));
const unsigned square_ids[] = {
  0, 1, /* Bottom */
  1, 2, /* Left */
  2, 3, /* Top */
  3, 0  /* Right */
};
const unsigned square_nprims = sizeof(square_ids)/(2*sizeof(unsigned));
static const double square_emi[] = {
  1.0, /* Bottom */
  1.0, /* Left */
  1.0, /* Top */
  1.0  /* Right */
};
static const double square_spec[] = {
  0.0, /* Bottom */
  0.0, /* Left */
  0.0, /* Top */
  0.0  /* Right */
};

/*******************************************************************************
 * Test function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sgf_device* sgf;
  struct sgf_scene* scn;
  struct sgf_scene_desc desc = SGF_SCENE_DESC_NULL;
  struct shape_context shape;
  unsigned n;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(sgf_device_create(NULL, &allocator, 1, &sgf) == RES_OK);

  CHK(sgf_scene_create(NULL, NULL) == RES_BAD_ARG);
  CHK(sgf_scene_create(sgf, NULL) == RES_BAD_ARG);
  CHK(sgf_scene_create(NULL, &scn) == RES_BAD_ARG);
  CHK(sgf_scene_create(sgf, &scn) == RES_OK);

  shape.emissivity = plane_emi;
  shape.specularity = plane_spec;
  shape.vertices = plane_verts;
  shape.nvertices = plane_nverts;
  shape.indices = plane_ids;
  shape.nprimitives = plane_nprims;
  shape.dim = 3;

  desc.get_position = get_position;
  desc.get_indices = get_indices;
  desc.get_emissivity = get_emissivity;
  desc.get_reflectivity = get_reflectivity;
  desc.get_specularity = get_specularity;
  desc.context = &shape;
  desc.nprims = (unsigned)plane_nprims;
  desc.nverts = (unsigned)plane_nverts;
  desc.nbands = 1;

  CHK(sgf_scene_setup_3d(NULL, NULL) == RES_BAD_ARG);
  CHK(sgf_scene_setup_3d(scn, NULL) == RES_BAD_ARG);
  CHK(sgf_scene_setup_3d(NULL, &desc) == RES_BAD_ARG);
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);

  CHK(sgf_scene_primitives_count(NULL, NULL) == RES_BAD_ARG);
  CHK(sgf_scene_primitives_count(scn, NULL) == RES_BAD_ARG);
  CHK(sgf_scene_primitives_count(NULL, &n) == RES_BAD_ARG);
  CHK(sgf_scene_primitives_count(scn, &n) == RES_OK);
  CHK(n == 2);

  desc.get_position = NULL;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.get_position = get_position;
  desc.get_indices = NULL;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.get_indices = get_indices;
  desc.get_emissivity = NULL;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.get_emissivity = get_emissivity;
  desc.get_reflectivity = NULL;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.get_reflectivity = get_reflectivity;
  desc.get_specularity = NULL;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.get_specularity = get_specularity;
  desc.nprims = 0;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.nprims = (unsigned)plane_nprims;
  desc.nverts = 0;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.nverts = (unsigned)plane_nverts;
  desc.nbands = 0;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  desc.nbands = 1;
  shape.emissivity = plane_emi_bad;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  shape.emissivity = plane_emi;
  shape.specularity = plane_spec_bad;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);
  shape.specularity = plane_spec;
  desc.get_absorption = get_absorption;
  shape.absorption = plane_abs;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_OK);
  shape.absorption = plane_abs_bad;
  CHK(sgf_scene_setup_3d(scn, &desc) == RES_BAD_ARG);

  CHK(sgf_scene_begin_integration(NULL) == RES_BAD_ARG);
  CHK(sgf_scene_begin_integration(scn) == RES_OK);
  CHK(sgf_scene_begin_integration(scn) == RES_BAD_OP);

  CHK(sgf_scene_end_integration(NULL) == RES_BAD_ARG);
  CHK(sgf_scene_end_integration(scn) == RES_OK);
  CHK(sgf_scene_end_integration(scn) == RES_BAD_OP);

  shape.emissivity = square_emi;
  shape.specularity = square_spec;
  shape.vertices = square_verts;
  shape.indices = square_ids;
  shape.nprimitives = square_nprims;
  shape.dim = 2;

  desc.nprims = (unsigned)square_nprims;
  desc.nverts = (unsigned)square_nverts;

  CHK(sgf_scene_begin_integration(scn) == RES_OK);
  CHK(sgf_scene_begin_integration(scn) == RES_BAD_OP);

  CHK(sgf_scene_end_integration(scn) == RES_OK);
  CHK(sgf_scene_end_integration(scn) == RES_BAD_OP);

  CHK(sgf_scene_ref_get(NULL) == RES_BAD_ARG);
  CHK(sgf_scene_ref_get(scn) == RES_OK);
  CHK(sgf_scene_ref_put(NULL) == RES_BAD_ARG);
  CHK(sgf_scene_ref_put(scn) == RES_OK);
  CHK(sgf_scene_ref_put(scn) == RES_OK);

  CHK(sgf_device_ref_put(sgf) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return RES_OK;
}
