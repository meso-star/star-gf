/* Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SGF_SCENE_C_H
#define SGF_SCENE_C_H

#include <rsys/dynamic_array_double.h>
#include <rsys/ref_count.h>
#include <rsys/rsys.h>

struct s2d_scene;
struct s2d_shape;
struct s3d_scene;
struct s3d_shape;
struct sgf_device;

struct sgf_scene {
  int dimensionality;

  union {
    struct {
      struct s2d_scene* scn;
      struct s2d_shape* shape;
      struct s2d_scene_view* view;
    } s2d;
    struct {
      struct s3d_scene* scn;
      struct s3d_shape* shape;
      struct s3d_scene_view* view;
    } s3d;
  } geometry;

  struct darray_double abs; /* Per primitive absorption */
  struct darray_double emi; /* Per primitive emissivity */
  struct darray_double refl; /* Per primitive reflectivity */
  struct darray_double spec; /* Per primitive specularity */
  unsigned nprims; /* #primitives */
  unsigned nbands; /* #spectral bands */
  int has_medium;

  ref_T ref;
  struct sgf_device* dev;
};


static FINLINE double
get_property__
  (const struct darray_double* property,
   const size_t nprims,
   const size_t nbands,
   const size_t iprim,
   const size_t iband)
{
  size_t i;
  ASSERT(iband < nbands && iprim < nprims && property);
  (void)nbands;
  i = iband * nprims + iprim;
  ASSERT(i < darray_double_size_get(property));
  return darray_double_cdata_get(property)[i];
}

static FINLINE double
scene_get_absorption
  (const struct sgf_scene* scn, const size_t iprim, const size_t iband)
{
  ASSERT(scn && scn->has_medium);
  return get_property__(&scn->abs, scn->nprims, scn->nbands, iprim, iband);
}

static FINLINE double
scene_get_emissivity
  (const struct sgf_scene* scn, const size_t iprim, const size_t iband)
{
  ASSERT(scn);
  return get_property__(&scn->emi, scn->nprims, scn->nbands, iprim, iband);
}

static FINLINE double
scene_get_reflectivity
  (const struct sgf_scene* scn, const size_t iprim, const size_t iband)
{
  ASSERT(scn);
  return get_property__(&scn->refl, scn->nprims, scn->nbands, iprim, iband);
}

static FINLINE double
scene_get_specularity
  (const struct sgf_scene* scn, const size_t iprim, const size_t iband)
{
  ASSERT(scn);
  return get_property__(&scn->spec, scn->nprims, scn->nbands, iprim, iband);
}

#endif /* SGF_SCENE_C_H */

