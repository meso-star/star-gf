# Star Gebhart Factor

The purpose of this C library is to numerically compute the [Gebhart
factor](https://en.wikipedia.org/wiki/Gebhart_factor) between 3D
primitives. The Gebhart factor *B(i,j)* between two arbitrary surfaces
*i* and *j* is defined as the fraction of the radiative flux emitted by
*i* that is absorbed by *j*. It is not equal to the [form
factor](https://en.wikipedia.org/wiki/View_factor) between *i* and *j*,
even tough the difference is subtle: the form factor is the fraction of
the radiative flux emitted by *i*, in direction of *j* (which is not
necessarily totally absorbed by *j*). In our case, Gebhart factors have
been integrated over *j*, which means that we compute the fraction of
the radiative flux emitted by *i* that is absorbed by all other
surfaces. It is obviously not equal to one, since a fraction of the flux
can be re-absorbed by *i* itself.

Star-GF estimates Gebhart factors with the Monte-Carlo method: it
consists in simulating the trajectory of energy bundles according to a
relevant physical model. Emission, absorption and reflection processes
are therefore taken into account, as well as spectral aspects (surface
properties can be defined as a function of wavelength). Should
scattering processes in semi-transparent media be taken into
consideration, it would be possible (even if actually not implemented).
The main advantage of this statistical method is the fact it provides a
numerical accuracy over each result: the Monte-Carlo method is often
referred as a reference method. Also, the resulting algorithm is totally
decoupled from the geometry: it will remain the same for every scene, no
matter how much data is used for its description. This geometry is
provided by the caller through a scene managed by the
[Star 3D](https://gitlab.com/meso-star/star-3d) library.

Gebhart factors may finally be used in order to compute the radiative
flux absorbed by each surface, and subsequently the net radiative flux
over each surface. The main advantage of Gebhart factors over form
factors is the associated flexibility in terms of reflection properties:
while form factors are computed with the implicit assumption that
surfaces are diffuse reflectors, there is no such restriction in the
case of Gebhart factors; any type of reflection can be used (totally or
partially specular surfaces).

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [Star 2D](https://gitlab.com/meso-star/star-2d)
- [Star 3D](https://gitlab.com/meso-star/star-3d)
- [Star SamPling](https://gitlab.com/meso-star/star-sp)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.3

Replace CMake with POSIX make as build system.

### Version 0.2.4

Sets the required version of Star-SampPling to 0.12. This version fixes
compilation errors with gcc 11 but introduces API breaks.

### Version 0.2.3

Fix API break on filter function introduced by Star-3D 0.8.

### Version 0.2.2

Bump the version of the dependencies and fix the code to handle their
updates.

### Version 0.2.1

Fix a crash that occurred in scenes having medium with a null
absorption. In such scenes, radiative flux going to infinity are not
allowed and must be handled as a numerical imprecision or a scene
inconsistency. This was not the case: this flux was accumulated into a
specific counter that was not initialised since, in this situation, such
flux could not exist.

### Version 0.2

- Add support of 2D scenes.
- Add support of the radiative flux absorption in 0D medium.

## License

Copyright (C) 2021, 2024 |Meso|Star> (contact@meso-star.com)  
Copyright (C) 2015-2018 EDF S.A., France (syrthes-support@edf.fr)

Star-GF is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
